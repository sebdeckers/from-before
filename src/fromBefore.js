export function fromBefore (pattern) {
  return this.includes(pattern)
    ? this.substring(this.indexOf(pattern))
    : this
}
