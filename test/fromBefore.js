import test from 'ava'
import {fromBefore} from '..'

test('chop before text', (t) => {
  t.is('foobar'::fromBefore('bar'), 'bar')
})

test('chop before emoji', (t) => {
  t.is('foo💩bar'::fromBefore('💩'), '💩bar')
})
